import requests
import paho.mqtt.client as mqtt
import os
import time

client = mqtt.Client()
client.connect(os.environ['MQTT_HOST'], int(os.environ['MQTT_PORT']), 60)
client.reconnect_delay_set(min_delay=10, max_delay=30)

while True:
    alerts = requests.get(
        f"{os.environ['ALERTMANAGER_URL']}/api/v2/alerts?active=true&silenced=false&inhibited=true&unprocessed=true").json()
    if (len(alerts) > 0):
        client.publish(os.environ['MQTT_SIREN_TOPIC'], 'ON')
    else:
        client.publish(os.environ['MQTT_SIREN_TOPIC'], 'OFF')
    time.sleep(30)
